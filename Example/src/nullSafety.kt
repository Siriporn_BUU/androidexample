package org.kotlinlang.play

fun main() {
    var neverNull: String = "This can't be null"

    neverNull = null
}