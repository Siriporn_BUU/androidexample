package org.kotlinlang.play

fun someCondition() = true

fun main() {
    var a: String = "initial"
    println(a)
    val b: Int = 1
    println(b)
    val c = 3
    println(c)
    var e: Int //set variable e be integer
    e = 123
    println(e)
    val d: Int
    if (someCondition()) {
        d = 1
    } else {
        d = 2
    }

    println(d)
}
